﻿using System;
using System.Collections.Generic;
using Expressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;

namespace Expressions
{
    /// <summary>
    /// Interaction logic for WPFFormulaBox.xaml
    /// </summary>
    public partial class WPFFormulaBox : UserControl, IDisposable
    {

        public static readonly DependencyProperty VariableMenuStructureProviderProperty =
            DependencyProperty.RegisterAttached(nameof(VariableMenuStructureProvider), typeof(IVariableMenuStructureProvider), typeof(WPFFormulaBox), new FrameworkPropertyMetadata(null,
            FrameworkPropertyMetadataOptions.AffectsRender
               | FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));
        public static readonly DependencyProperty FormulaProperty =
            DependencyProperty.RegisterAttached(nameof(Formula), typeof(SerializableFormula), typeof(WPFFormulaBox), new FrameworkPropertyMetadata(null,
            FrameworkPropertyMetadataOptions.AffectsRender
               | FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, new PropertyChangedCallback(FormulaPropertyChangedHandler)));
        private Stack<SolidColorBrush> _history = new Stack<SolidColorBrush>();
        private Boolean _MutePropertyChangedEvent = false;
        private Boolean _MuteTextBoxChangedEvent = false;
        private List<Tag> _tags = new List<Tag>();
        private SyntaxCheckResult _syntaxCheckResult = null;

        private readonly SolidColorBrush[] BRACKETCOLORS =
        {
            Brushes.Blue,
            Brushes.Brown,
            Brushes.Green,
            Brushes.Orange,
            Brushes.Turquoise
        };

        public WPFFormulaBox()
        {
            UseBuildInErrorIndicator = true;
            InitializeComponent();
            rtbFormula.TextChanged += rtbFormula_TextChanged;
        }

        private void ApplyColors()
        {
            _MuteTextBoxChangedEvent = true;
            try
            {
                var textRange = new TextRange(rtbFormula.Document.ContentStart, rtbFormula.Document.ContentEnd);
                textRange.ClearAllProperties();
                _tags.Clear();
                SetBracketColors();
                ApplyErrorMarkers();
                //apply layout on text
                foreach (var t in _tags)
                {
                    var range = new TextRange(t.StartPosition, t.EndPosition);
                    range.ApplyPropertyValue(TextElement.ForegroundProperty, t.TextColor);
                    range.ApplyPropertyValue(TextElement.BackgroundProperty, t.BackGroundColor);
                    range.ApplyPropertyValue(TextElement.FontWeightProperty,
                                             t.IsBold ? FontWeights.Bold : FontWeights.Normal);
                }
            }
            finally
            {
                _MuteTextBoxChangedEvent = false;
            }
        }

        private void ApplyErrorMarkers()
        {
            if (_syntaxCheckResult == null) return;
            var navigator = rtbFormula.Document.ContentStart;
            var allTextIndex = 0;
            var markerLength = _syntaxCheckResult.ExpressionErrorToken.Length;
            while (navigator.CompareTo(rtbFormula.Document.ContentEnd) < 0)
            {
                var context = navigator.GetPointerContext(LogicalDirection.Backward);
                if (context == TextPointerContext.ElementStart && navigator.Parent is Run)
                {
                    var run = (Run)navigator.Parent;
                    var text = run.Text;
                    var textIndex = 0;
                    foreach (var ch in text)
                    {
                        if ((_syntaxCheckResult.ExpressionErrorPosition <= allTextIndex) & (markerLength > 0))
                        {
                            SetColorOfText(run,
                                           textIndex,
                                           1,
                                           Brushes.White,
                                           Brushes.Red,
                                           false);
                            markerLength--;
                        }
                        textIndex++;
                        allTextIndex++;
                    }
                }
                navigator = navigator.GetNextContextPosition(LogicalDirection.Forward);
            }
        }

        private void CheckErrors(Boolean aForceRecoloring)
        {
            if (_syntaxCheckResult == null)
            {
                vbWarning.Visibility = Visibility.Collapsed;
                if (aForceRecoloring) ApplyColors();
            }
            else
            {
                if (UseBuildInErrorIndicator)
                {
                    vbWarning.Visibility = Visibility.Visible;
                    vbWarning.ToolTip = _syntaxCheckResult.ExpressionError;
                }
                else
                {
                    vbWarning.Visibility = Visibility.Collapsed;
                }
                ApplyColors();
            }
        }

        private void Context_Click(object sender, RoutedEventArgs e)
        {
            if (sender is MenuItem)
            {
                MenuItem menuItem = (MenuItem)sender;
                if (menuItem.Tag != null)
                {
                    if (menuItem.Tag is string)
                    {
                        string text = (string)menuItem.Tag;
                        this.rtbFormula.CaretPosition.InsertTextInRun(text);
                        var newCartePos = this.rtbFormula.CaretPosition.GetPositionAtOffset(text.Length);
                        if (newCartePos == null) newCartePos = this.rtbFormula.Document.ContentEnd;
                        this.rtbFormula.CaretPosition = newCartePos;
                    }
                }
            }
        }
        private void lbCodeCompletion_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                if (lbCodeCompletion.SelectedIndex == -1)
                {
                    lbCodeCompletion.SelectedIndex = 0;
                }
                IFormulaVariable formulaVariable = (IFormulaVariable)this.lbCodeCompletion.SelectedItem;
                if (formulaVariable != null)
                {
                    string name = formulaVariable.Name;
                    this.rtbFormula.CaretPosition.InsertTextInRun(name);
                    this.rtbFormula.CaretPosition = this.rtbFormula.CaretPosition.GetPositionAtOffset(name.Length);
                    this.puFormula.IsOpen = false;
                    this.rtbFormula.Focus();
                }
            }
            if (e.Key == Key.Escape)
            {
                this.puFormula.IsOpen = false;
                this.rtbFormula.Focus();
            }
        }

        private void rtbFormula_KeyDown(object sender, KeyEventArgs e)
        {
            bool flag = Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl);
            if (flag)
            {
                bool flag2 = e.Key == Key.Space;
                if (flag2)
                {
                    TextPointer end = this.rtbFormula.Selection.End;
                    bool flag3 = end == null;
                    if (!flag3)
                    {
                        Rect characterRect = end.GetCharacterRect(LogicalDirection.Forward);
                        Point point = this.rtbFormula.PointToScreen(characterRect.TopRight);
                        var popup = this.puFormula;
                        popup.PlacementRectangle = new Rect(point.X, point.Y + this.rtbFormula.ActualHeight + 5.0, 100.0, 100.0);
                        this.lbCodeCompletion.SelectedIndex = 0;
                        popup.IsOpen = true;
                        var listBoxItem = (ListBoxItem)lbCodeCompletion
                                                       .ItemContainerGenerator

                                                       .ContainerFromItem(lbCodeCompletion.SelectedItem);
                        listBoxItem?.Focus();
                    }
                }
            }
        }

        private void rtbFormula_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (_MuteTextBoxChangedEvent) return;
            _MutePropertyChangedEvent = true;
            try
            {
                var textRange = new TextRange(rtbFormula.Document.ContentStart, rtbFormula.Document.ContentEnd);
                if (Formula.CultureSpecificFormulaText == textRange.Text.TrimEnd()) return;
                Formula.CultureSpecificFormulaText = textRange.Text.TrimEnd();
                _syntaxCheckResult = Formula.VerifySyntax();
                //ApplyColors();
                CheckErrors(true);
            }
            finally
            {
                _MutePropertyChangedEvent = false;
            }
        }

        private void SetBracketColors()
        {
            var bracketIndex = -1;
            var navigator = rtbFormula.Document.ContentStart;
            while (navigator.CompareTo(rtbFormula.Document.ContentEnd) < 0)
            {
                var context = navigator.GetPointerContext(LogicalDirection.Backward);
                if (context == TextPointerContext.ElementStart && navigator.Parent is Run)
                {
                    var run = (Run)navigator.Parent;
                    var text = run.Text;
                    var textIndex = 0;
                    foreach (var ch in text)
                    {
                        if (ch == '(')
                        {
                            bracketIndex++;
                            var bracketColor = BRACKETCOLORS[bracketIndex % 5];
                            SetColorOfText(run, textIndex, 1, bracketColor, Brushes.White, true);
                            _history.Push(bracketColor);
                        }
                        else if (ch == ')')
                        {
                            if (_history.Count > 0)
                            {
                                var bracketColor = _history.Pop();
                                SetColorOfText(run, textIndex, 1, bracketColor, Brushes.White, true);
                                bracketIndex--;
                            }
                            else
                            {
                                SetColorOfText(run,
                                               textIndex,
                                               1,
                                               Brushes.Black,
                                               Brushes.Red,
                                               false);
                            }
                        }
                        textIndex++;
                    }
                }
                navigator = navigator.GetNextContextPosition(LogicalDirection.Forward);
            }
        }

        private void SetColorOfText(Run aRun,
                                    int aStart,
                                    int aLength,
                                    Brush aColor,
                                    Brush aBackColor,
                                    Boolean aBold)
        {
            var t = new Tag();
            t.StartPosition = aRun.ContentStart.GetPositionAtOffset(aStart, LogicalDirection.Forward);
            t.EndPosition = aRun.ContentStart.GetPositionAtOffset(aStart + aLength, LogicalDirection.Backward);
            t.IsBold = aBold;
            t.TextColor = aColor;
            t.BackGroundColor = aBackColor;
            _tags.Add(t);
        }

        private void UpdateFormula(SerializableFormula aNewFormula)
        {
            if (aNewFormula == null)
            {
                throw new ArgumentNullException("The Formula Property must not be null");
            }
            _MuteTextBoxChangedEvent = true;
            SetValue(FormulaProperty, aNewFormula);
            var cc = new CompositeCollection();
            var ccc1 = new CollectionContainer();
            ccc1.Collection = (new List<BuildInFunction>() { new BuildInFunction("IF", "Conditional IF function"), new BuildInFunction("CASE", "Conditional CASE function") });
            cc.Add(ccc1);
            var ccc2 = new CollectionContainer();
            var availableFunctions = Formula.GetAllFunctions();
            ccc2.Collection = availableFunctions;
            cc.Add(ccc2);
            miFunctions.ItemsSource = cc;
            lbCodeCompletion.ItemsSource = availableFunctions;
            var t = (Formula.FormulaText == null) ? String.Empty : Formula.CultureSpecificFormulaText;
            rtbFormula.Document.Blocks.Clear();
            rtbFormula.Document.Blocks.Add(new Paragraph(new Run(t)));
            CheckErrors(true);
            _MutePropertyChangedEvent = false;
        }

        private void CreateVariablesMenuItems()
        {
            miVariables.Items.Clear();
            var variablesMenuItemsLookup = new Dictionary<String, MenuItem>();
            foreach (var i in Formula.GetAvailableVariables())
            {
                if (this.VariableMenuStructureProvider == null)
                {
                    var mi = new MenuItem()
                    {
                        Tag = i.Name,
                        Header = new TextBlock { Text = i.Name }
                    };
                    mi.Click += Context_Click;
                    miVariables.Items.Add(mi);
                }
                else
                {
                    var posName = this.VariableMenuStructureProvider.ProvidePosition(i);
                    if ((String.IsNullOrEmpty(posName)) || (posName == "/"))
                    {
                        var mi = new MenuItem()
                        {
                            Tag = i.Name,
                            Header = new TextBlock { Text = i.Name }
                        };
                        mi.Click += Context_Click;
                        miVariables.Items.Add(mi);
                    }
                    else
                    {
                        if (posName.StartsWith("/"))
                        {
                            var parts = posName.Split('/');
                            var combined = String.Empty;
                            MenuItem mi = miVariables;
                            foreach (var item in parts)
                            {
                                combined += item;
                                if (!String.IsNullOrEmpty(combined))
                                {
                                    if (!variablesMenuItemsLookup.ContainsKey(combined))
                                    {
                                        var smi = new MenuItem { Header = item };
                                        variablesMenuItemsLookup.Add(combined, smi);
                                        mi.Items.Add(smi);
                                    }
                                    mi = variablesMenuItemsLookup[combined];
                                }
                            }
                            var n = new MenuItem()
                            {
                                Tag = i.Name,
                                Header = new TextBlock { Text = i.Name }
                            };
                            n.Click += Context_Click;
                            mi.Items.Add(n);
                        }
                        else
                        {
                            throw new ArgumentException("Position Strings should start with /");
                        }
                    }
                }
            }
        }

        public static void FormulaPropertyChangedHandler(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue == null) return;
            ((WPFFormulaBox)sender).UpdateFormula((SerializableFormula)e.NewValue);
        }


        public SerializableFormula Formula
        {
            get
            {
                var f = (SerializableFormula)GetValue(FormulaProperty);
                if (f == null)
                {
                    f = new SerializableFormula();
                    return f;
                }
                return f;
            }
            set
            {
                UpdateFormula(value);
            }
        }

        public Boolean UseBuildInErrorIndicator
        { get; set; }

        public IVariableMenuStructureProvider VariableMenuStructureProvider
        {
            get
            {
                return (IVariableMenuStructureProvider)GetValue(VariableMenuStructureProviderProperty);
            }
            set
            {
                SetValue(VariableMenuStructureProviderProperty, value);
            }
        }

        new struct Tag
        {
            public TextPointer StartPosition;
            public TextPointer EndPosition;
            public Brush TextColor;
            public Brush BackGroundColor;
            public Boolean IsBold;
        }

        class BuildInFunction
        {
            public String Description { get; set; }
            public String Name { get; set; }
            public BuildInFunction(String aName, String aDescription)
            {
                Name = aName;
                Description = aDescription;
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    
                }
                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }
        #endregion

        private void ContextMenu_Opened(object sender, RoutedEventArgs e)
        {
            CreateVariablesMenuItems();
        }
    }

    public class PathStyle : Style
    {
        public PathStyle()
        {
            TargetType = typeof(System.Windows.Shapes.Path);
        }
    }

    public interface IVariableMenuStructureProvider
    {
        /// <summary>
        /// This method returns the position of the item in the menu
        /// </summary>
        /// <param name="aMenuItemName"></param>
        /// <returns>/ is the root
        /// /SomeSubmenu will put the item in the submenu named SomeSubMenu</returns>
        String ProvidePosition(IFormulaVariable aMenuItemName);
    }
}
