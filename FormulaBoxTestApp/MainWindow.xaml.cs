﻿using Expressions;
using PropertyChanged;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace FormulaBoxTestApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    [AddINotifyPropertyChangedInterface]
    public partial class MainWindow : Window
    {  
        private SerializableFormula ff;
        public SerializableFormula sf
        {
            get; set;
        }

        public ObservableCollection<FormulaWrapper> SerializableFormulas
        {
            get; set;
        }


        public IVariableMenuStructureProvider StructureProvider
        {
            get; set;
        }

        public MainWindow()
        {
            this.DataContext = this;
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //StructureProvider = new TestVariableMenuStructureProvider();
            //ff = new SerializableFormula();
            //ff.FormulaResultType = TResultType.Float;
            //var a = new FormulaVariable("a", 5);
            //var b = new FormulaVariable("b", 4);
            //var c = new FormulaVariable("c", 3);
            //ff.AvailableVariables.Add(a);
            //ff.AvailableVariables.Add(b);
            //ff.AvailableVariables.Add(c);
            ff.AddFunction(new Expressions.Functions.TAverageFunction());
            ff.AddFunction(new Expressions.Functions.TExpFunction());
            var formula = "(var1_RAW + var2_RAW) * var3_VAL + var5";
            ff.FormulaText = formula;
            sf = ff;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            //  var f = new SerializableFormula();
            //  f.FormulaResultType = TResultType.Float;
            //  var a = new FormulaVariable("a_RAW", 5);
            //  var b = new FormulaVariable("b_RAW", 4);
            //  var c = new FormulaVariable("c_VAL", 3);
            // // f.AvailableVariables.Add(a);
            // // f.AvailableVariables.Add(b);
            ////  f.AvailableVariables.Add(c);
            //  f.AvailableFunctions.Add(new Expressions.Functions.TAverageFunction());
            //  f.AvailableFunctions.Add(new Expressions.Functions.TExpFunction());
            //  var formula = "(a - b) / c";
            //  f.FormulaText = formula;

            //  sf = f;

            var a = new FormulaVariable("var1_RAW", 5);
            var b = new FormulaVariable("var2_RAW", 4);
            var c = new FormulaVariable("var3_VAL", 3);
            var d = new FormulaVariable("var4_VAL", 3);
            var g = new FormulaVariable("var5", 3);

            if (ff != null)
            {
                if (!ff.GetAvailableVariables().Any(v => v.Name == "var1_RAW"))
                {
                    ff.AddVariable(a);
                    ff.AddVariable(b);
                    ff.AddVariable(c);
                    ff.AddVariable(d);
                    ff.AddVariable(g);
                }
            }
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            ((System.ComponentModel.INotifyPropertyChanged)this).PropertyChanged += MainWindow_PropertyChanged;
            StructureProvider = new TestVariableMenuStructureProvider();
            ff = new SerializableFormula();
            ff.FormulaResultType = TResultType.Float;
        }

        private void MainWindow_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            Console.WriteLine("prop change");
        }

        private void DataGrid_BeginningEdit(object sender, DataGridBeginningEditEventArgs e)
        {
            // Check if the DataContext for the row being edited is valid.
            if (e.Row.DataContext == null)
            {
                // Cancel the edit if the DataContext is not valid.
                e.Cancel = true;
            }
        }


        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            SerializableFormulas = new ObservableCollection<FormulaWrapper>();
            var f = new SerializableFormula();
            f.FormulaResultType = TResultType.Float;
            var a = new FormulaVariable("a_RAW", 5);
            var b = new FormulaVariable("b_RAW", 4);
            var c = new FormulaVariable("c_VAL", 3);
            f.AddVariable(a);
            f.AddVariable(b);
            f.AddVariable(c);
            f.AddFunction(new Expressions.Functions.TAverageFunction());
            f.AddFunction(new Expressions.Functions.TExpFunction());
            var formula = "(a_RAW - b_RAW) / c_VAL";
            f.FormulaText = formula;
            var fw = new FormulaWrapper() { Formula = f, StructureProvider = this.StructureProvider };
            SerializableFormulas.Add(fw);

            f = new SerializableFormula();
            f.FormulaResultType = TResultType.Float;
            a = new FormulaVariable("a_RAW", 5);
            b = new FormulaVariable("b_RAW", 4);
            c = new FormulaVariable("c_VAL", 3);
            f.AddVariable(a);
            f.AddVariable(b);
            f.AddVariable(c);
            f.AddFunction(new Expressions.Functions.TAverageFunction());
            f.AddFunction(new Expressions.Functions.TExpFunction());
            formula = "(a_RAW + b_RAW) / c_VAL";
            f.FormulaText = formula;

            fw = new FormulaWrapper() { Formula = f, StructureProvider = this.StructureProvider };
            SerializableFormulas.Add(fw);

            f = new SerializableFormula();
            f.FormulaResultType = TResultType.Float;
            a = new FormulaVariable("a_RAW", 5);
            b = new FormulaVariable("b_RAW", 4);
            c = new FormulaVariable("c_VAL", 3);
            f.AddVariable(a);
            f.AddVariable(b);
            f.AddVariable(c);
            f.AddFunction(new Expressions.Functions.TAverageFunction());
            f.AddFunction(new Expressions.Functions.TExpFunction());
            formula = "(a_RAW * b_RAW) / c_VAL";
            f.FormulaText = formula;
            fw = new FormulaWrapper() { Formula = f, StructureProvider = this.StructureProvider };
            SerializableFormulas.Add(fw);

        }
    }

    public class TestVariableMenuStructureProvider : IVariableMenuStructureProvider
    {
        public string ProvidePosition(IFormulaVariable aMenuItemName)
        {
            if (aMenuItemName.Name.EndsWith("_EFF_GRAY")) return "/Gray Level Values/Effective Values/";
            if (aMenuItemName.Name.EndsWith("_MEAN_GRAY")) return "/Gray Level Values/Mean Values";
            if (aMenuItemName.Name.EndsWith("_MEDIAN_GRAY")) return "/Gray Level Values/Median Values";
            if (aMenuItemName.Name.EndsWith("_STDDEV_GRAY")) return "/Gray Level Values/StdDev Values";
            if (aMenuItemName.Name.EndsWith("_VAL")) return "/Calculated Values";
            return null;
        }

    }

    public class InterpretationVariablesStructureProvider : IVariableMenuStructureProvider
    {
        public string ProvidePosition(IFormulaVariable aMenuItemName)
        {
            if (aMenuItemName.Name.EndsWith("_EFF_GRAY")) return "/Gray Level Values/Effective Values/";
            if (aMenuItemName.Name.EndsWith("_MEAN_GRAY")) return "/Gray Level Values/Mean Values";
            if (aMenuItemName.Name.EndsWith("_MEDIAN_GRAY")) return "/Gray Level Values/Median Values";
            if (aMenuItemName.Name.EndsWith("_STDDEV_GRAY")) return "/Gray Level Values/StdDev Values";
            if (aMenuItemName.Name.EndsWith("_VAL")) return "/Calculated Values";
            return null;
        }

    }

    public static class StructureProvider
    {
        private static IVariableMenuStructureProvider _interpretationVariablesStructureProvider;
        private static Object syncObject = new Object();

        public static IVariableMenuStructureProvider InterpretationVariablesStructureProvider
        {
            get
            {
                if (_interpretationVariablesStructureProvider == null)
                {
                    lock (syncObject)
                    {
                        if (_interpretationVariablesStructureProvider == null)
                        {
                            _interpretationVariablesStructureProvider = new InterpretationVariablesStructureProvider();
                        }
                    }
                }
                return _interpretationVariablesStructureProvider;
            }
        }
    }

    public class FormulaWrapper
    {
        public SerializableFormula Formula
        {
            get; set;
        }

        public IVariableMenuStructureProvider StructureProvider
        {
            get; set;
        }
    }
}
